(* 
This program gives a generalized solution to the problem:

"Find the largest prime factor of a given number n greater than zero"
*)


(* this function returns true if n is prime, that is, is not divisible by none of the primes of
   the div_prime_l low-to-high ordered list, searching up to its sqrt rounded up; false otherwise.
   See README for the mathematical justification.  it requires that div_prime_l contains 
   enough primes to test n *)

let try_prime n div_prime_l =

  let sqrt_n = (truncate(sqrt(float n))+1) in

    let rec try_prime_rec p_l =

      (* if the list is exausted, then no prime is a divisor for n *)
      if (p_l = []) then true
      else let p = (List.hd p_l) in

        (* if the prime is > sqrt(n), the list is ordered, so n is prime *)
        if (p > sqrt_n) then true

        else if (n mod p = 0) then false

          else try_prime_rec (List.tl p_l) 
    in
      try_prime_rec div_prime_l;;



(* This function builds the low-to-high ordered list of prime numbers less or equal to n, for n>=2,    and returns it.
   To achieve an increase in speed, the list starts already with 2 in it and iterates only 
   on odd numbers; in this way it will cut the number of steps in half *)

let build_prime_list n = 

  let rec build_prime_list_rec i prime_l = 

    if (i > n) then prime_l 

    else if try_prime i prime_l then 

      (* i is prime: add it to the end of the list and iterate on the next odd number *)
      build_prime_list_rec (i + 2) (prime_l@[i]) 

      else
        (* try adding the next odd number to the list *)
        build_prime_list_rec (i + 2) prime_l

    (* starts with list containing 2 and with 3 as candidate *)
    in build_prime_list_rec 3 [2];;
     


(* this function computes the highest prime factor of a given number n>0  and returns it. 

   It first computes the ordered list of the primes up to sqrt(n) rounded up 
   (in this way the last match will be the highest match, and if the sqrt is prime is 
   included in the list).

   Then starts decomposing n trying to divide it for each prime number in the candidate list, 
   several times if possible, until the highest prime factor is reached or the prime list is empty.

   If the list is empty, it means that or n is 1 (special case), or the remainder is higher than 
   sqrt(n) and is prime (see README) *) 

let prime_factor n = 

  let prime_cand_l = build_prime_list (truncate(sqrt(float n))+1) in

    let rec prime_match remainder prime_l = 

      (* if the list is empty, remainder is prime  and is > sqrt(n) *)
      if (prime_l = []) then remainder 

      else let prime_num = List.hd prime_l in

        if (remainder = prime_num) then 
          (* the rest of all the previous divisions is the highest prime factor because the list
          is ordered low-to-high *)
          prime_num 
     
        else if (remainder mod prime_num = 0) then 

          (* iterates on remainder / prime_num on the same prime candidate list *)
          prime_match (remainder / prime_num) prime_l 
                 
          else 
          (* remainder is not divisible by prime_num; iterates on the rest of the list *)
          prime_match remainder (List.tl prime_l) 

      in prime_match n prime_cand_l;;



(* this function print the usage instructions. Takes as argument a string as program name *)

let print_usage pname =
      Printf.printf "usage : %s n\nwith n>0\n" pname;;



(* Main program. Takes one argument on the command line.
   Print usage instructions if invoked without arguments or with bad arguments eg. float or strings *)

let main () =

  let progname = Sys.argv.(0) in 

    try
      let n = int_of_string Sys.argv.(1) in 

      if (n > 0) then
      begin 
        print_int (prime_factor n); 
        print_newline () 
      end
      else
        print_usage progname

    with _ -> print_usage progname;;

main ();;
